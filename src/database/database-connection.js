/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 08:46:27
 * @FilePath: /sign-in-apps/src/database/database-connection.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 08:44:54
 * @FilePath: /signInApp/src/database/database-connection.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import * as SQLite from 'expo-sqlite';
import Datastore from 'react-native-local-mongodb'
import AsyncStorage from '@react-native-async-storage/async-storage'

// const db = new Datastore({inMemoryOnly: true, storage: AsyncStorage});
// export default db

export const DatabaseConnection = {
  getConnection: () => SQLite.openDatabase("database.db"),
};