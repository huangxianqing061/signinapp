/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-13 08:45:51
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 09:30:06
 * @FilePath: /sign-in-apps/src/database/databse-operator.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { DatabaseConnection } from '../database/database-connection';

const db = DatabaseConnection.getConnection();

export const DatabaseOperator = {
    initTables: () => {
        db.transaction((tx) => {
            // drop table 
            // tx.executeSql('DROP TABLE IF EXISTS user', []);
            // tx.executeSql('DROP TABLE IF EXISTS sign_in', []);
            tx.executeSql(
                "SELECT name FROM sqlite_master WHERE type='table' AND name='user'",
                [],
                (tx, result) => {
                    console.log('table user is ', result.rows);
                    if (result.rows.length == 0) {
                        tx.executeSql('DROP TABLE IF EXISTS user', []);
                        tx.executeSql(
                            'CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(20), password INT(100))',
                            []
                        );
                        tx.executeSql('DROP TABLE IF EXISTS sign_in', []);
                        tx.executeSql(
                            'CREATE TABLE IF NOT EXISTS sign_in(id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(20), time VARCHAR(20), longitude VARCHAR(100), latitude VARCHAR(100))',
                            []
                        );
                    }
                    console.log('table user and table sign_in are recreated');
                }
            );
        });
    },
    dropTables: (tx) => {

    },
};