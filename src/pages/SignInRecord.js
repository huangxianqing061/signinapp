/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 11:13:46
 * @FilePath: /signInApp/src/pages/SignInRecord.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useState, useEffect } from 'react';
import { FlatList, Text, View, SafeAreaView, StyleSheet } from 'react-native';
import { DatabaseConnection } from '../database/database-connection';
import moment from 'moment';

const db = DatabaseConnection.getConnection();

const ViewAllUser = () => {
  let [flatListItems, setFlatListItems] = useState([]);

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM sign_in ORDER BY time DESC',
        [],
        (tx, results) => {
          setFlatListItems(results.rows);
        }
      );
    });
  }, []);

  let listItemView = (item) => {
    return (
      <View
        key={item.id}
        style={{ backgroundColor: '#e6e6e6', marginTop: 20, padding: 30, borderRadius: 10, display: 'flex' }}>
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.textheader}>用户名称</Text>
          <Text style={styles.textbottom}> {item.username}</Text>
        </View>
        <View style={{ flexDirection: "row", marginTop: '8px' }}>
          <Text style={styles.textheader}>签到时间</Text>
          <Text style={styles.textbottom}> {moment(item.time).format('YYYY年MM月DD日HH:mm:ss')}</Text>
        </View>
        <View style={{ flexDirection: "row", marginTop: '8px' }}>
          <Text style={styles.textheader}>签到地点</Text>
          <Text style={styles.textbottom}> {item.longitude + ',' + item.latitude}</Text>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <FlatList
            contentContainerStyle={{ paddingHorizontal: 20 }}
            data={flatListItems}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => listItemView(item)}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textheader: {
    color: '#666',
    fontSize: 18

  },
  textbottom: {
    color: '#333',
    fontSize: 18,
  },
});

export default ViewAllUser;