/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 08:28:01
 * @FilePath: /signInApp/src/pages/Login.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useState } from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  SafeAreaView,
  Text,
} from 'react-native';

import MyText from './components/MyText';
import MyTextInput from './components/MyTextInput';
import MyButton from './components/MyButton';
import { DatabaseConnection } from '../database/database-connection';

const db = DatabaseConnection.getConnection();

const UpdateUser = ({ navigation }) => {
  let [username, setUsername] = useState('');
  let [userPassword, setuserPassword] = useState('');
  let updateUser = () => {
    console.log( username, userPassword);
    if (!username) {
      alert('请输入用户名');
      return;
    }
    if (!userPassword) {
      alert('请输入密码');
      return;
    }
    db.transaction(function (tx) {
      tx.executeSql(
        'SELECT * FROM user where username = ?',
        [username],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rows.length > 0) {
            localStorage.setItem("username", username)
            alert('登录成功');
            window.location.reload()
            navigation.navigate('Home')
          } else {
            alert('找不到该用户');
            return
          };
        }
      );
    });
  };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="handled">
            <KeyboardAvoidingView
              behavior="padding"
              style={{ flex: 1, justifyContent: 'space-between' }}>
              <MyTextInput
                placeholder="请输入用户名"
                value={username}
                style={{ padding: 14 }}
                onChangeText={
                  (username) => setUsername(username)
                }
              />
              <MyTextInput
                placeholder="请输入密码"
                value={userPassword}
                onChangeText={
                  (userPassword) => setuserPassword(userPassword)
                }
                maxLength={10}
                style={{ padding: 14 }}
                keyboardType="numeric"
                secureTextEntry={true}
              />
              <MyButton
                title="登录"
                customClick={updateUser}
              />
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default UpdateUser;