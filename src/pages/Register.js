/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 08:07:44
 * @FilePath: /sign-in-apps/src/pages/Register.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useState } from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  SafeAreaView,
} from 'react-native';
import MyTextInput from './components/MyTextInput';
import MyButton from './components/MyButton';
import { DatabaseConnection } from '../database/database-connection';

// console.log('8888',db)
const db = DatabaseConnection.getConnection();

const Register = ({ navigation }) => {
  let [username, setUsername] = useState('');
  let [userPassword, setuserPassword] = useState('');
  let register_user = () => {
    console.log(username, userPassword);
    if (!username) {
      alert('请输入用户名');
      return;
    }
    if (!userPassword) {
      alert('请输入密码');
      return;
    }
    db.transaction(function (tx) {
      tx.executeSql(
        'SELECT * FROM user where username = ?',
        [username],
        (tx, results) => {
          if (results.rows.length > 0) {
            console.log('该用户已经被注册');
            alert('该用户已经被注册');
            return;
          } else {
            registerFun()
          }
        }
      );
    });
  };
  const registerFun = () => {
    db.transaction(function (tx) {
      tx.executeSql(
        'INSERT INTO user (username, password) VALUES (?,?)',
        [username, userPassword],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            alert('注册成功');
            navigation.navigate('Login')
          };
        }
      );
    });
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="handled">
            <KeyboardAvoidingView
              behavior="padding"
              style={{ flex: 1, justifyContent: 'space-between' }}>
              <MyTextInput
                placeholder="请输入账号"
                onChangeText={
                  (username) => setUsername(username)
                }
                style={{ padding: 14 }}
              />
              <MyTextInput
                placeholder="请输入密码"
                onChangeText={
                  (userPassword) => setuserPassword(userPassword)
                }
                maxLength={10}
                keyboardType="password"
                style={{ padding: 14 }}
                secureTextEntry={true}
              />
              <MyButton title="注册" customClick={register_user} />
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Register;