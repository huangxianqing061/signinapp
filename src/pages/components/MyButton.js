/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 08:07:04
 * @FilePath: /sign-in-apps/src/pages/components/MyButton.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const MyButton = (props) => {
  return (
    <TouchableOpacity
      style={!props.disabled ? styles.button : styles.disabledButton}
      onPress={props.customClick}>
      
      <Text style={styles.text}>
        {props.title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#2992C4',
    color: '#ffffff',
    padding: 16,
    marginTop: 300,
    marginLeft: 35,
    marginRight: 35,
    borderRadius: 5,
  },
  disabledButton: {
    alignItems: 'center',
    backgroundColor: '#a9a9a9',
    color: '#d3d3d3',
    padding: 16,
    marginTop: 300,
    marginLeft: 35,
    marginRight: 35,
    borderRadius: 5,
  },
  text: {
    color: '#ffffff',
  },
});

export default MyButton;