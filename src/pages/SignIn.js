/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 10:12:27
 * @FilePath: /signInApp/src/pages/SignIn.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useEffect, useState } from 'react';
import { Text, View, SafeAreaView, Image } from 'react-native';
import MyText from './components/MyText';
import MyTextInput from './components/MyTextInput';
import MyButton from './components/MyButton';
import { DatabaseConnection } from '../database/database-connection';
import moment from 'moment';

const db = DatabaseConnection.getConnection();

// 1994-11-05T13:15:30Z
// YYYY-MM-DDTHH:mm:ssZ
const getTimeString = (time, HHmmss) => {
  let mom = moment(time);
  return mom.format('YYYY-MM-DDT') + HHmmss + mom.format('Z');
}

const getStartEndTime = (time) => {
  const morning = { start: "00:00:00", end: "12:00:00" };
  const noon = { start: "12:00:00", end: "13:00:00" };
  const afternoon = { start: "13:00:00", end: "24:00:00" };
  let now = moment(time).format('HH:mm:ss');
  if (morning.start <= now && now < morning.end) {
    morning.start = getTimeString(time, morning.start);
    morning.end = getTimeString(time, morning.end);
    return morning;
  } else if (noon.start <= now && now < noon.end) {
    noon.start = getTimeString(time, noon.start);
    noon.end = getTimeString(time, noon.end);
    return noon;
  } else {
    afternoon.start = getTimeString(time, afternoon.start);
    afternoon.end = getTimeString(time, afternoon.end);
    return afternoon;
  }
}

const ViewUser = () => {
  // time
  let [time, setTime] = useState();
  // location
  let [longitude, setLongitude] = useState();
  let [latitude, setLatitude] = useState();
  useEffect(() => {
    console.log('use effect...')
    setTime(new Date())
    const t = setInterval(() => {
      setTime(new Date())
    }, 1000);

    navigator.geolocation.watchPosition(
      location => {
        setLongitude(location.coords.longitude);
        setLatitude(location.coords.latitude);
        console.log('get location success...')
        return;
      }
    );

    return () => {
      clearTimeout(t)
    }
  }, []);
  // sign in
  const handleSignIn = () => {
    if (!(time && latitude && longitude)) {
      return;
    }
    let startEndTime = getStartEndTime(time);
    let username = localStorage.getItem('username');
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM sign_in where username = ? and ? <= time and time < ?',
        [username, startEndTime.start, startEndTime.end],
        (tx, results) => {
          if (results.rows.length > 0) {
            // 提示“不允许重复签到”
            alert('同一时段不允许重复签到');
          } else {
            let utcTimeString = moment(time).format('YYYY-MM-DDTHH:mm:ssZ');
            tx.executeSql(
              'INSERT INTO sign_in (username, time,longitude,latitude) VALUES (?, ?,?,?)',
              [username, utcTimeString, longitude, latitude],
              (tx, results) => {
                results
                alert('签到成功');
              },
              (tx, sqlError) => {
                console.error('insert sign_in table error', sqlError)
              }
            );
          }
        },
        (tx, sqlError) => {
          console.error('select from sign_in table error', sqlError)
        }
      );
    });
  };


  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <View
            style={{
              marginLeft: 35,
              marginRight: 35,
              marginTop: 10
            }}>
            <View><Text style={{ color: '#333', fontSize: '26px' }}>{moment(time).format('YYYY年MM月DD日HH:mm:ss')}</Text></View>
            <View style={{ marginTop: '30px' }}><Image
              style={{ width: '60px', height: '60px',marginLeft:'-22px' }}
              source={require('../../assets/locationIcon.png')}
            /><Text style={{ color: '#333', fontSize: '26px' }}>{longitude ? longitude + ',' + latitude : "获取中..."}</Text></View>
          </View>
          {longitude ? (<MyButton title="签到" customClick={handleSignIn} disabled={false} />) : <MyButton title="签到" disabled={true} />}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ViewUser;