/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-10 20:28:40
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-13 09:18:31
 * @FilePath: /sign-in-apps/src/pages/Home.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useEffect } from 'react';
import { View, SafeAreaView, Image } from 'react-native';
import MyImageButton from './components/MyImageButton';
import { DatabaseOperator } from '../database/database-operator';
import MyButton from './components/MyButton';

const Home = ({ navigation }) => {
  let username = localStorage.getItem('username');
  useEffect(() => {
    DatabaseOperator.initTables();
  }, []);
  const handleDel = () => {
    alert("确认退出吗？")
    localStorage.clear()
    window.location.reload()
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, backgroundColor: "#fff" }}>
            {username ? (<View><MyImageButton
              title="签到"
              btnColor='#F9AD29'
              btnMargin='50vh'
              btnIcon="user"
              customClick={() => navigation.navigate('SignIn')}
            />
              <MyImageButton
                title="签到记录"
                btnColor='#384F62'
                btnIcon="users"
                customClick={() => navigation.navigate('SignInRecord')}
              />
              <MyImageButton
                title="注销"
                btnColor='#D1503A'
                btnIcon="user-times"
                customClick={() => handleDel()}
              /></View>) : (<View style={{ display: 'flex' }}><View><MyImageButton
                title="注册"
                btnColor='#2992C4'
                btnMargin='60vh'
                btnIcon="user-plus"
                customClick={() => navigation.navigate('Register')}
              />
              </View>
                <View>
                  <MyImageButton
                    title="登录"
                    btnColor='#2992C4'
                    btnIcon="user-circle"
                    customClick={() => navigation.navigate('Login')}
                  />
                </View></View>)}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Home;